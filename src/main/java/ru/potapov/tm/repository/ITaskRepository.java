package ru.potapov.tm.repository;

import ru.potapov.tm.entity.Project;

import java.util.Collection;

public interface ITaskRepository<T> extends IRepository<T> {
    public Collection findAll(String userId, String idProject);
}
