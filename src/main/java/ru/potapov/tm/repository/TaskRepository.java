package ru.potapov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;
import java.util.*;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository<Task> {
    @NotNull
    @Override
    public Collection findAll() {
        return super.findAll();
    }

    @Nullable
    @Override
    public Task findOne(@NotNull final String name) {
        return super.findOne(name);
    }

    @Override
    public void persist(@NotNull final Task task) {
        super.persist(task);
    }

    @NotNull
    @Override
    public Task merge(@NotNull final Task task) {
        return super.merge(task);
    }

    @Override
    public void remove(@NotNull final Task task) {
        super.remove(task);
    }

    @Override
    public void removeAll() {
        super.removeAll();
    }

    @Override
    public void removeAll(@NotNull final Collection<Task> list) {
        super.removeAll(list);
    }

    @NotNull
    @Override
    public Collection<Task> getCollection(){
        return super.getCollection();
    }

    @NotNull
    @Override
    public Collection findAll(@NotNull final String userId) {
        return super.findAll(userId);
    }

    @NotNull
    @Override
    public Collection findAll(@NotNull final String userId, @NotNull final String idProject) {
        Collection<Task> listTask = new ArrayList<>();
        for (Task task : getCollection()) {
            if (task.getProjectId().equals(idProject)){
                listTask.add(task);
            }
        }
        return listTask;
    }

    @Nullable
    @Override
    public Task findOne(@NotNull final String userId, @NotNull final String name) {
        return super.findOne(userId, name);
    }

    @Override
    public void persist(@NotNull final String userId, @NotNull final Task task) {
        super.persist(userId, task);
    }

    @NotNull
    @Override
    public Task merge(@NotNull final String userId, @NotNull final Task task) {
        return super.merge(userId, task);
    }

    @NotNull
    @Override
    public void remove(@NotNull final String userId, @NotNull final Task task) {
        super.remove(userId, task);
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        super.removeAll(userId);
    }

    @Override
    public void removeAll(@NotNull final String userId, @NotNull final Collection<Task> list) {
        super.removeAll(userId, list);
    }

    @NotNull
    @Override
    public Collection<Task> getCollection(@NotNull final String userId){
        return super.getCollection(userId);
    }
}
