package ru.potapov.tm.repository;

import ru.potapov.tm.entity.User;

import java.util.Collection;
import java.util.Map;

public interface IRepository<T> {
    Collection<T> findAll();
    Collection<T> findAll(String userId);
    T findOne(String name);
    T findOne(String userId, String name);
    T findOneById(String id);
    T findOneById(String userId, String id);
    void persist(T t);
    void persist(String userId, T t);
    T merge(T tNew);
    T merge(String userId, T tNew);
    void remove(T t);
    void remove(String userId, T t);
    void removeAll();
    void removeAll(String userId);
    void removeAll(Collection<T> list);
    void removeAll(String userId, Collection<T> list);
    Collection<T> getCollection();
    Collection<T> getCollection(String userId);
}
