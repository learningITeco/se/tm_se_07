package ru.potapov.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.Entity;
import ru.potapov.tm.entity.User;


import java.util.*;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractRepository<T extends Entity> {
    @NotNull private Map<String, T> mapRepository = new HashMap<>();

    public void setMapRepository(@NotNull Map<String, T> mapRepository) {this.mapRepository = mapRepository;}

    @NotNull
    public Map<String, T> getMapRepository() {return mapRepository;}

    //CRUD
    @NotNull
    public Collection<T> findAll() {
        return getMapRepository().values();
    }

    @NotNull
    public Collection<T> findAll(final String userId) {
        return getCollection(userId);
    }

    @Nullable
    public T findOne(@NotNull final String name) {
        for (@NotNull final T t : getMapRepository().values()) {
            if (t.getName().equals(name)){
                return t;
            }
        }
        return null;
    }

    @Nullable
    public T findOne(@NotNull final String userId, @NotNull final String name) {
        for (@NotNull final T t : getMapRepository().values()) {
            if (t.getName().equals(name) && t.getUserId().equals(userId))
                return t;
        }
        return null;
    }

    @Nullable
    public T findOneById(@NotNull final String id) {
        for (@NotNull final T t : getMapRepository().values()) {
            if (t.getId().equals(id))
                return t;
        }
        return null;
    }

    @Nullable
    public T findOneById(@NotNull final String userId, @NotNull final String id) {
        for (@NotNull final T t : getMapRepository().values()) {
            if (t.getId().equals(id) && t.getUserId().equals(userId))
                return t;
        }
        return null;
    }

    public void persist(@NotNull final T t) {
        getMapRepository().putIfAbsent(t.getId(), t);
    }

    public void persist(@NotNull final String userId, @NotNull final T t) {
        if (t.getUserId().equals(userId))
            getMapRepository().putIfAbsent(t.getId(), t);
    }

    @NotNull
    public T merge(@NotNull final T t) {
        getMapRepository().put(t.getId(), t);
        return t; //we need old reference, after merge
    }

    @NotNull
    public T merge(@NotNull final String userId, @NotNull final T t) {
        if (t.getUserId().equals(userId))
            getMapRepository().put(t.getId(), t);
        return t; //we need old reference, after merge
    }

    public void remove(@NotNull final T t) {
        getMapRepository().remove(t.getId());
    }

    public void remove(@NotNull final String userId, @NotNull final T t) {
        if (t.getUserId().equals(userId))
            getMapRepository().remove(t.getId());
    }

    public void removeAll() {
        getMapRepository().clear();
    }

    public void removeAll(@NotNull final Collection<T> list) {
        for (@NotNull final T t : list) {
            getMapRepository().remove(t.getId());
        }
    }

    public void removeAll(@NotNull final String userId) {
        for (@NotNull final T t : getMapRepository().values()) {
            if (t.getUserId().equals(userId))
                getMapRepository().remove(t.getId());
        }
    }

    public void removeAll(@NotNull final String userId, @NotNull final Collection<T> list) {
        for (@NotNull final T t : list) {
            if (t.getUserId().equals(userId))
                getMapRepository().remove(t.getId());
        }
    }

    @NotNull
    public Collection<T> getCollection(){
        return getMapRepository().values();
    }

    @NotNull
    public Collection<T> getCollection(@NotNull final String userId){
        Collection<T> collections = new ArrayList<>();
        for (@NotNull final T t : mapRepository.values()) {
            if (t.getUserId().equals(userId))
                collections.add(t);
        }
        return collections;
    }

}


