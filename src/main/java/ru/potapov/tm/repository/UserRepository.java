package ru.potapov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.User;
import java.util.Collection;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class UserRepository extends AbstractRepository<User> implements IUserRepository<User> {
    @NotNull
    @Override
    public Collection<User> findAll() {
        return super.findAll();
    }

    @Nullable
    @Override
    public User findOne(@NotNull final String name) {
        return super.findOne(name);
    }

    @Nullable
    @Override
    public User findOneById(@NotNull final String id) {
        return super.findOneById(id);
    }

    @Override
    public void persist(@NotNull final User user) {
        super.persist(user);
    }

    @NotNull
    @Override
    public User merge(@NotNull final User user) {
        return super.merge(user);
    }

    @Override
    public void remove(@NotNull final User user) {
        super.remove(user);
    }

    @Override
    public void removeAll() {
        super.removeAll();
    }

    @Override
    public void removeAll(@NotNull final Collection<User> list) {
        super.removeAll(list);
    }

    @NotNull
    @Override
    public Collection<User> getCollection(){
        return super.getCollection();
    }
}
