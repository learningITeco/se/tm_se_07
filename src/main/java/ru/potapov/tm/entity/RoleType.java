package ru.potapov.tm.entity;

public enum RoleType {
    Administrator,
    User;
}
