package ru.potapov.tm.entity;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class Task implements Cloneable, Entity  {
    @Nullable private String      id;
    @Nullable private String      name;
    @Nullable private String      description;
    @Nullable private String      projectId;
    @Nullable private String      userId;
    @Nullable private LocalDateTime   dateStart;
    @Nullable private LocalDateTime   dateFinish;

    public Task(@NotNull String name) {
        this();
        this.name = name;
    }

    @Override
    public String toString() {
        return "Task ["+getName()+"]";
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
