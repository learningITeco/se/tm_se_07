package ru.potapov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.command.*;
import ru.potapov.tm.repository.*;
import ru.potapov.tm.service.*;
import java.util.*;



import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class  Bootstrap implements ServiceLocator {

    @NotNull private IProjectRepository projectRepository     = new ProjectRepository();
    @NotNull private ITaskRepository taskRepository           = new TaskRepository();
    @NotNull private IUserRepository userRepository           = new UserRepository();

    @NotNull private IProjectService projectService           = new ProjectService(projectRepository);
    @NotNull private ITaskService taskService                 = new TaskService(taskRepository, projectRepository);
    @NotNull private IUserService userService                 = new UserService(userRepository);
    @NotNull private ITerminalService terminalService         = new TerminalService(this);

    public Bootstrap() {}

    public void init(@NotNull final Class[] CLASSES){
        try {
            start();
        }catch (Exception e){
            terminalService.printlnArbitraryMassage("Something went wrong...");
            e.printStackTrace();
        }
    }

    private void start() throws Exception{
        terminalService.printlnArbitraryMassage("*** WELCOME TO TASK MANAGER! ***");
        String command = "";
        while (!"exit".equals(command)){
            command = terminalService.readLine("\nInsert your command in low case or command <help>:");
            execute(command);
        }
    }

    private void execute(@Nullable String command) throws Exception{
        if ( Objects.isNull(command) || command.isEmpty())
            return;

        AbstractCommand abstractCommand = terminalService.getMapCommands().get(command);

        if (Objects.isNull(abstractCommand))
            return;

        abstractCommand.execute();
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public ITerminalService getTerminalService() {
        return terminalService;
    }
}