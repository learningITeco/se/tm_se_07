package ru.potapov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;

import java.util.Collection;

@NoArgsConstructor
public abstract class TaskReadCommandAbstract extends AbstractCommand {
    public TaskReadCommandAbstract(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @NotNull
    @Override
    public abstract String getName();

    @NotNull
    @Override
    public abstract String getDescription();

    @Override
    public void execute(){
        if (!super.allowedRun())
            return;
    };

    public void printTasksOfProject(@NotNull Project project, @NotNull Collection<Task> listTasks){
        getServiceLocator().getTerminalService().printlnArbitraryMassage("");
        getServiceLocator().getTerminalService().printlnArbitraryMassage("Project [" + project.getName() + "]:");
        for (Task task : listTasks) {
            if (task.getProjectId().equals(project.getId())){
                @NotNull String taskInfo =  getServiceLocator().getTaskService().collectTaskInfo(project, task, getServiceLocator().getTerminalService().getFt());
                getServiceLocator().getTerminalService().printlnArbitraryMassage(taskInfo);
            }
        }
    }
}
