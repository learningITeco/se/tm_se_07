package ru.potapov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.potapov.tm.bootstrap.Bootstrap;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class TaskDeleteAllCommand extends AbstractCommand {
    public TaskDeleteAllCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-delete-all";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Deletes all tasks of all projects";
    }

    @Override
    public void execute() {
        if (!super.allowedRun())
            return;
        getServiceLocator().getTaskService().removeAll(getServiceLocator().getUserService().getAuthorizedUser().getId());
        getServiceLocator().getTerminalService().printlnArbitraryMassage("All tasks of all projects have deleted");
    }
}
