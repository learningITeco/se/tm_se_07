package ru.potapov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.entity.User;

import javax.xml.bind.DatatypeConverter;
import java.util.Objects;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class UserUpdateCommand extends AbstractCommand {
    public UserUpdateCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @NotNull
    @Override
    public String getName() {
        return "user-update";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Updates a user password";
    }

    @Override
    public void execute() {
        if (!super.allowedRun())
            return;
        @Nullable User findUser = null;

        boolean circleForName = true;
        while (circleForName){
            @NotNull String name = getServiceLocator().getTerminalService().readLine("Input a user name for update:");

            if ("exit".equals(name)){
                return;
            }

            findUser = getServiceLocator().getUserService().getUserByName(name);

            if (Objects.isNull(findUser)) {
                getServiceLocator().getTerminalService().printlnArbitraryMassage("User with name [" + name + "] is not exist, plz try again or type command <exit>");
                continue;
            }

            circleForName = false;
        }


        @NotNull String pass = getServiceLocator().getTerminalService().readLine("Input a new password for user ["+ findUser.getLogin() + "]:");
        try {
            getServiceLocator().getUserService().changePass(findUser, DatatypeConverter.printHexBinary(getServiceLocator().getUserService().getMd().digest(pass.getBytes())) );
        }catch (Exception e) {e.printStackTrace();}
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
