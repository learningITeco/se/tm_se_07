package ru.potapov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.entity.User;

import java.util.Objects;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class UserReadCommand extends AbstractCommand {
    public UserReadCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @NotNull
    @Override
    public String getName() {
        return "user-read";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Reads a user in detail";
    }

    @Override
    public void execute() {
        if (!super.allowedRun())
            return;
        @Nullable User findUser = null;
        boolean circleForProject = true;
        while (circleForProject) {
            @NotNull String name = getServiceLocator().getTerminalService().readLine("Input a user name:");

            if ("exit".equals(name)){
                return;
            }

            findUser = getServiceLocator().getUserService().getUserByName(name);

            if (Objects.isNull(findUser)) {
                getServiceLocator().getTerminalService().printlnArbitraryMassage("User with name [" + name + "] does not exist, plz try again or type command <exit>");
                continue;
            }

            circleForProject = false;
        }
        String userInfo = getServiceLocator().getUserService().collectUserInfo(findUser);
        getServiceLocator().getTerminalService().printlnArbitraryMassage(userInfo);
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
