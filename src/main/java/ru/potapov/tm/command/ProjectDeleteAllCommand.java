package ru.potapov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.potapov.tm.bootstrap.Bootstrap;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectDeleteAllCommand extends AbstractCommand {
    public ProjectDeleteAllCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @NotNull
    @Override
    public String getName() {
        return "projects-delete-all";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Deletes all project";
    }

    @Override
    public void execute() {
        if (!super.allowedRun())
            return;
        getServiceLocator().getTaskService().removeAll( getServiceLocator().getUserService().getAuthorizedUser().getId() );
        getServiceLocator().getProjectService().removeAll( getServiceLocator().getUserService().getAuthorizedUser().getId() );
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
