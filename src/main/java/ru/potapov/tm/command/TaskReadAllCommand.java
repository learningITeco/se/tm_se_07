package ru.potapov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.entity.Project;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class TaskReadAllCommand extends TaskReadCommandAbstract {
    public TaskReadAllCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-read-all";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Reads all tasks";
    }

    @Override
    public void execute() {
        if (!super.allowedRun())
            return;
        if (getServiceLocator().getTaskService().checkSize() == 0) {
            getServiceLocator().getTerminalService().printlnArbitraryMassage("We do not have any tasks");
            return;
        }

        for (Project project : getServiceLocator().getProjectService().getCollection( getServiceLocator().getUserService().getAuthorizedUser().getId() )) {
            printTasksOfProject(project, getServiceLocator().getTaskService().findAll(getServiceLocator().getUserService().getAuthorizedUser().getUserId(), project.getId()));
        }
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
