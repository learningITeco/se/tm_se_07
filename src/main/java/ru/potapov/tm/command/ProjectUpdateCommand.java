package ru.potapov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.entity.Project;

import java.util.Objects;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectUpdateCommand extends AbstractCommand {
    public ProjectUpdateCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-update";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update name of the Nth project";
    }

    @Override
    public void execute() {
        if (!super.allowedRun())
            return;
        @Nullable Project findProject = null;
        boolean circleForName = true;
        while (circleForName){
            getServiceLocator().getTerminalService().printlnArbitraryMassage("Input a project name for update:");
            @NotNull String name = getServiceLocator().getTerminalService().getIn().nextLine();

            if ("exit".equals(name)){
                return;
            }

            findProject = getServiceLocator().getProjectService().findProjectByName(name);

            if (Objects.isNull(findProject)) {
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Project with name [" + name + "] is not exist, plz try again or type command <exit>");
                continue;
            }

            if (!getServiceLocator().getUserService().getAuthorizedUser().getId().equals(findProject.getUserId())){
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Project with name [" + name + "] does not belong to you!, plz try again or type command <exit>");
                continue;
            }

            circleForName = false;
        }

        getServiceLocator().getTerminalService().printlnArbitraryMassage("Do you want to update the name? <y/n>");
        @NotNull String answer = getServiceLocator().getTerminalService().getIn().nextLine();
        if ("y".equals(answer) || "Y".equals(answer)){
            getServiceLocator().getTerminalService().printlnArbitraryMassage("Input a new name for this task:");

            try {
                getServiceLocator().getProjectService().renameProject(findProject, getServiceLocator().getTerminalService().getIn().nextLine());
            }catch (Exception e) {e.printStackTrace();}
            getServiceLocator().getTerminalService().printMassageCompleted();
        }
    }
}
