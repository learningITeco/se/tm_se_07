package ru.potapov.tm.command;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.bootstrap.ServiceLocator;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractCommand {
    private boolean needAuthorize           = false;
    @Nullable protected Bootstrap bootstrap;
    @NotNull private ServiceLocator serviceLocator = bootstrap;

    public AbstractCommand(@Nullable final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    public abstract void execute();

    public boolean allowedRun(){
        if ( needAuthorize && !getServiceLocator().getUserService().isAuthorized() ){
            getServiceLocator().getTerminalService().printMassageNotAuthorized();
            return false;
        }
        return true;
    }
}
