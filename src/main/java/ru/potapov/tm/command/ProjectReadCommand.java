package ru.potapov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.User;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectReadCommand extends AbstractCommand {
    public ProjectReadCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-read";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Reads all projects";
    }

    @Override
    public void execute() {
        if (!super.allowedRun())
            return;
        getServiceLocator().getTerminalService().printlnArbitraryMassage("All projects: \n");
        if (getServiceLocator().getProjectService().checkSize() == 0){
            getServiceLocator().getTerminalService().printlnArbitraryMassage("We have not any project");
            return;
        }

        @Nullable User owner = null;
        for (Project project : getServiceLocator().getProjectService().getCollection( getServiceLocator().getUserService().getAuthorizedUser().getId() )) {
            owner = getServiceLocator().getUserService().getUserById(project.getUserId());

            if (!getServiceLocator().getUserService().getAuthorizedUser().getId().equals(project.getUserId())){
                continue;
            }

            String projectInfo = getServiceLocator().getProjectService().collectProjectInfo(project, owner.getLogin(), getServiceLocator().getTerminalService().getFt());
            getServiceLocator().getTerminalService().printlnArbitraryMassage(projectInfo);
        }
    }
}
