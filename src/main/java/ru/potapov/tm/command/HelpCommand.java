package ru.potapov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.potapov.tm.bootstrap.Bootstrap;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class HelpCommand extends AbstractCommand {
    public HelpCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Lists all command";
    }

    @Override
    public void execute() {
        if (!super.allowedRun())
            return;
        for (final AbstractCommand command : getServiceLocator().getTerminalService().getListCommands()) {
            getServiceLocator().getTerminalService().printlnArbitraryMassage(command.getName() + ": " + command.getDescription());
        }
    }
}
