package ru.potapov.tm.service;

import ru.potapov.tm.entity.RoleType;
import ru.potapov.tm.entity.User;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.util.Collection;
import java.util.UUID;

public interface IUserService {
    User create(String name, String hashPass, RoleType role);
    User getUserByName(String name);
    User getUserById(String id);
    boolean isUserPassCorrect(User user, String hashPass);
    Collection<User> getCollectionUser();
    User changePass(User user, String newHashPass) throws CloneNotSupportedException;
    void put(User user);
    String collectUserInfo(User user);
    void createPredefinedUsers();
    MessageDigest getMd();
    boolean isAuthorized();
    void setAuthorized(boolean authorized);
    User getAuthorizedUser();
    void setAuthorizedUser(User authorizedUser);
}
