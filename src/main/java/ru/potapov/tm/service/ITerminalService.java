package ru.potapov.tm.service;

import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;

public interface ITerminalService {
    void regestry(AbstractCommand command);
    Collection<AbstractCommand> getListCommands();
    Map<String, AbstractCommand> getMapCommands();
    LocalDateTime inputDate(String massage);
    Scanner getIn() ;
    DateTimeFormatter getFt() ;
    String readLine(String msg);
    void printMassageNotAuthorized();
    void printMassageCompleted();
    void printMassageOk();
    void printlnArbitraryMassage(String msg);
    void printArbitraryMassage(String msg);
}
