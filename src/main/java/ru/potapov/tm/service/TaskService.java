package ru.potapov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.repository.IProjectRepository;
import ru.potapov.tm.repository.ITaskRepository;

import java.time.format.DateTimeFormatter;
import java.util.*;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class TaskService implements ITaskService {
    @Nullable private ITaskRepository<Task> taskRepository;
    @Nullable private IProjectRepository<Project> projectRepository;

    public TaskService(@NotNull final ITaskRepository<Task> taskRepository, @NotNull final IProjectRepository<Project> projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    public ITaskRepository<Task> getTaskRepository() {
        return taskRepository;
    }

    public int checkSize(){
         return taskRepository.getCollection().size();
    }

    @Nullable
    @Override
    public Task findTaskByName(@NotNull final String name){
        Task result = null;

        for (@NotNull final Task task : taskRepository.getCollection()) {
            if (task.getName().equals(name)) {
                result = task;
                break;
            }
        }
        return result;
    }

    @Override
    public void remove(@NotNull final Task task){
        taskRepository.remove(task);
    }

    @Override
    public void removeAll(@NotNull final String userId){
        taskRepository.removeAll(userId);
    }

    @Override
    public void removeAll(@NotNull final Collection<Task> listTasks){
        taskRepository.removeAll(listTasks);
    }

    @NotNull
    @Override
    public Task renameTask(@NotNull final Task task, @NotNull final String name) throws CloneNotSupportedException{

        if (Objects.nonNull(name)){
            Task newTask = (Task) task.clone();
            newTask.setName(name);
            return taskRepository.merge(newTask);
        }
        return task;
    }

    @Override
    public void changeProject(@NotNull final Task task, @Nullable final Project project) throws CloneNotSupportedException{
        if (Objects.nonNull(project)){
            Task newTask = (Task) task.clone();
            newTask.setProjectId(project.getId());
            taskRepository.merge(newTask);
        }
    }

    @NotNull
    @Override
    public Collection<Task> findAll(@NotNull final String userId, @NotNull final String idProject){
        return taskRepository.findAll(userId, idProject);
    }

    @NotNull
    @Override
    public Collection<Task> findAll(@NotNull final String idProject){
        return taskRepository.findAll(idProject);
    }

    @Override
    public void put(@NotNull final Task task){
        taskRepository.merge(task);
    }

    @NotNull
    @Override
    public String collectTaskInfo(@NotNull final Project project, @NotNull final Task task, @NotNull final DateTimeFormatter ft){
        String res = "";

        res += "\n";
        res += "    Task [" + task.getName() + "]" +  "\n";
        res += "    Description: " + task.getDescription() + "\n";
        res += "    ID: " + task.getId() + "\n";
        res += "    Date start: " + task.getDateStart().format(ft) + "\n";
        res += "    Date finish: " + task.getDateFinish().format(ft) + "\n";

        return res;
    }
}
