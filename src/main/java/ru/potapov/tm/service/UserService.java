package ru.potapov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.RoleType;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.repository.IUserRepository;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.UUID;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class UserService implements IUserService {
    @Nullable private IUserRepository<User> userRepository;
    @Nullable private MessageDigest md     = null;

    @Nullable private User authorizedUser  = null;
    private boolean isAuthorized           = false;

    public UserService() {
        //digest (MD5):
        try {
            md = MessageDigest.getInstance("MD5");
        }catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }
    }

    public UserService(@Nullable final IUserRepository<User> userRepository) {
        this();
        this.userRepository = userRepository;
        createPredefinedUsers();
    }

    @NotNull
    @Override
    public User create(@Nullable final String name, @Nullable final String hashPass, @Nullable final RoleType role){
        User user = new User();
        user.setRoleType(role);
        user.setLogin(name);
        user.setHashPass(hashPass);
        user.setId(UUID.randomUUID().toString());

        userRepository.persist(user);

        return user;
    }

    @Nullable
    @Override
    public User getUserByName(@Nullable final String name){
        return userRepository.findOne(name);
    }

    @Nullable
    @Override
    public User getUserById(@Nullable final String id){
        return userRepository.findOneById(id);
    }

    @Override
    public boolean isUserPassCorrect(@Nullable final User user, @Nullable final String hashPass){
        boolean res = false;

        //if ( Arrays.equals(user.getHashPass(), hashPass) )
        if (user.getHashPass().equals(hashPass))
            res = true;

        return res;
    }

    @NotNull
    @Override
    public Collection<User> getCollectionUser(){
        return userRepository.getCollection();
    }

    @NotNull
    @Override
    public User changePass(@Nullable final User user, @Nullable final String newHashPass) throws CloneNotSupportedException{
        User newUser = (User) user.clone();
        newUser.setHashPass(newHashPass);
        return userRepository.merge(newUser);
    }

    @Override
    public void put(@Nullable final User user){
        userRepository.persist(user);
    }

    @NotNull
    @Override
    public String collectUserInfo(@Nullable final User user){
        String res = "";

        res += "\n";
        res += "    User [" + user.getLogin() + "]" +  "\n";
        res += "    ID: " + user.getId() + "\n";
        res += "    Role: " + user.getRoleType() + "\n";

        return res;
    }

    @Override
    public void createPredefinedUsers() {
        String hashPass;
        byte [] hashPassByte;

        hashPassByte = md.digest("1".getBytes());
        hashPass = DatatypeConverter.printHexBinary(hashPassByte);
        create("user",  hashPass, RoleType.User);

        hashPassByte = md.digest("2".getBytes());
        hashPass = DatatypeConverter.printHexBinary(hashPassByte);
        create("admin", hashPass, RoleType.Administrator);
    }
}
