package ru.potapov.tm.service;

import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.repository.IProjectRepository;

import java.time.format.DateTimeFormatter;
import java.util.Collection;

public interface IProjectService {
    IProjectRepository getProjectRepository();
    int checkSize();
    Project findProjectByName(String name);
    Project findProjectByName(String userId, String name);
    Collection<Project> getCollection(String userId);
    Project renameProject(Project project, String name) throws CloneNotSupportedException;
    void removeAll(String userId);
    void removeAll(Collection<Project> listProjects);
    void remove(Project project);
    void put(Project project);
    String collectProjectInfo(Project project, String owener, DateTimeFormatter ft);
}
