package ru.potapov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class TerminalService implements ITerminalService {
    @NotNull final Map<String, AbstractCommand> mapCommand = new LinkedHashMap<>();
    @Nullable private Bootstrap bootstrap;

    @NotNull final private DateTimeFormatter   ft            = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    @NotNull final private Scanner             in            = new Scanner(System.in);

    //Constants
    @NotNull final private String YY                         = "Y";
    @NotNull final private String Yy                         = "y";

    public TerminalService(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        @Nullable AbstractCommand command = null;
        command = new DelimiterCommand(null); regestry(command);
        command = new ExitCommand(bootstrap); regestry(command);
        command = new HelpCommand(bootstrap); regestry(command);
        command = new AboutCommand(bootstrap); regestry(command);
        command = new DelimiterCommand(null); regestry(command);
        command = new ProjectCreateCommand(bootstrap); command.setNeedAuthorize(true); regestry(command);
        command = new ProjectReadCommand(bootstrap); command.setNeedAuthorize(true); regestry(command);
        command = new ProjectUpdateCommand(bootstrap); command.setNeedAuthorize(true); regestry(command);
        command = new ProjectDeleteCommand(bootstrap); command.setNeedAuthorize(true); regestry(command);
        command = new ProjectDeleteAllCommand(bootstrap); command.setNeedAuthorize(true); regestry(command);
        command = new DelimiterCommand(null); regestry(command);
        command = new TaskCreateCommand(bootstrap); command.setNeedAuthorize(true); regestry(command);
        command = new TaskReadAllCommand(bootstrap); command.setNeedAuthorize(true); regestry(command);
        command = new TaskReadCommand(bootstrap); command.setNeedAuthorize(true); regestry(command);
        command = new TaskUpdateCommand(bootstrap); command.setNeedAuthorize(true); regestry(command);
        command = new TaskDeleteCommand(bootstrap); command.setNeedAuthorize(true); regestry(command);
        command = new TaskDeleteForProjectCommand(bootstrap); command.setNeedAuthorize(true); regestry(command);
        command = new TaskDeleteAllCommand(bootstrap); command.setNeedAuthorize(true); regestry(command);
        command = new DelimiterCommand(null); regestry(command);

        command = new UserAuthorizeCommand(bootstrap);regestry(command);
        command = new UserExitCommand(bootstrap); command.setNeedAuthorize(true); regestry(command);

        command = new UserCreateCommand(bootstrap); command.setNeedAuthorize(true); regestry(command);
        command = new UserReadAllCommand(bootstrap); regestry(command);
        command = new UserReadCommand(bootstrap); regestry(command);
        command = new UserUpdateCommand(bootstrap); command.setNeedAuthorize(true); regestry(command);
        command = new UserDeleteCommand(bootstrap); command.setNeedAuthorize(true); regestry(command);
    }

    @Override
    public void regestry(@Nullable final AbstractCommand command){
        if ( Objects.isNull(command))
            return;

        @Nullable final String commandName        = command.getName();
        @Nullable final String commandDescription = command.getDescription();

        if (Objects.isNull(commandName) || Objects.isNull(commandDescription)
                || commandName.isEmpty() || commandDescription.isEmpty())
            return;

        command.setServiceLocator(bootstrap);
        mapCommand.put(commandName, command);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getListCommands(){
        return mapCommand.values();
    }

    @NotNull
    @Override
    public Map<String, AbstractCommand> getMapCommands(){
        return mapCommand;
    }

    @NotNull
    @Override
    public LocalDateTime inputDate(String massage){
        printlnArbitraryMassage(massage);
        String strDate = in.nextLine();
        LocalDateTime date;
        try {
            int year = Integer.parseInt(strDate.substring(6)),
                    month= Integer.parseInt(strDate.substring(3,5)),
                    day  = Integer.parseInt(strDate.substring(0,2));
            date = LocalDateTime.of(year,month, day,0,0);
        }catch (Exception e){
            printlnArbitraryMassage("Error formate date! Date set to the end of year.");
            date = LocalDateTime.of(LocalDateTime.now().getYear(),12,31,23,59);
        }

        return date;
    }

    @NotNull
    @Override
    public String readLine(String msg){
        System.out.println(msg);
        String user = (bootstrap.getUserService().isAuthorized()) ? bootstrap.getUserService().getAuthorizedUser().getLogin() + ": " : "";
        System.out.print(user);
        return in.nextLine();
    }

    @Override
    public void printMassageNotAuthorized(){
        printlnArbitraryMassage("You are not authorized, plz login (type command <user-login>)");
    }

    @Override
    public void printMassageCompleted(){
        printlnArbitraryMassage("Completed");
    }

    @Override
    public void printMassageOk(){
        printlnArbitraryMassage("Ok");
    }

    @Override
    public void printlnArbitraryMassage(String msg){
        printArbitraryMassage(msg + "\n");
    }

    @Override
    public void printArbitraryMassage(String msg){
        System.out.print(msg);
    }
}
