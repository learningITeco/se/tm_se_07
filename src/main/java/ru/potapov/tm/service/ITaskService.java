package ru.potapov.tm.service;

import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.repository.ITaskRepository;

import java.time.format.DateTimeFormatter;
import java.util.Collection;

public interface ITaskService {
    ITaskRepository<Task> getTaskRepository();
    int checkSize();
    Task findTaskByName(String name);
    void remove(Task task);
    void removeAll(String userId);
    void removeAll(Collection<Task> listTasks);
    Task renameTask(Task task, String name) throws CloneNotSupportedException;
    void changeProject(Task task, Project project) throws CloneNotSupportedException;
    Collection<Task> findAll(String idProject);
    Collection<Task> findAll(String userId, String idProject);
    void put(Task task);
    String collectTaskInfo(Project project, Task task, DateTimeFormatter ft);
}
