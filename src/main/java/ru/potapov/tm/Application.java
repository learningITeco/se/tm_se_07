package ru.potapov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;

/**
 * Application
 * v 1.0.7
 */
import lombok.Getter;
import lombok.Setter;
import ru.potapov.tm.command.*;

@Getter
@Setter
public final class Application {
    @NotNull final private String version = "1.0.7";

    @NotNull private static final Class[] CLASSES = {
    ExitCommand.class, HelpCommand.class, AboutCommand.class, ProjectCreateCommand.class,
    ProjectReadCommand.class,     ProjectUpdateCommand.class, ProjectDeleteCommand.class,
    ProjectDeleteAllCommand.class,     TaskCreateCommand.class,    TaskReadAllCommand.class,
    TaskReadCommand.class,     TaskUpdateCommand.class,   TaskDeleteCommand.class,
    TaskDeleteForProjectCommand.class,     TaskDeleteAllCommand.class,    UserAuthorizeCommand.class,
    UserExitCommand.class,    UserCreateCommand.class,    UserReadAllCommand.class,
    UserReadCommand.class,     UserUpdateCommand.class,    UserDeleteCommand.class
    };

    public Application() {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(CLASSES);
    }

    public static void main(String[] args) {
        new Application();
    }
}
